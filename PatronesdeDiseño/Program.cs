﻿using PatronesdeDiseño.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatronesdeDiseño
{
    class Program
    {
        static void Main(string[] args)
        {

            //Creo el objeto original
            Smartphone smart = new Smartphone("Samsung","5.5 pulgadas","negro","S10");
            //Creo el objeto clonado
            Smartphone smartphone = (Smartphone)smart.Clone();

            smartphone.SetMarca("Huawei");
            smartphone.SetVersion("P30");

            Console.WriteLine("Objeto Original: " + smart.GetCaracteristicas());
            Console.WriteLine("Objeto Clonado: "+ smartphone.GetCaracteristicas());
            Console.ReadKey();
        }
    }
}
