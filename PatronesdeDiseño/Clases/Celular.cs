﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatronesdeDiseño.Clases
{
    abstract class Celular
    {
        private string marca;
        private string dimension;
        private string color;
        private string version;

        public Celular(string marca,string dimension,string color, string version)
        {
            this.marca = marca;
            this.dimension = dimension;
            this.color = color;
            this.version = version;
        }

        protected Celular(string marca)
        {
            this.marca = marca;
        }

        public string GetMarca()
        {
            return this.marca;
        }

        public void SetMarca(string marca)
        {
            this.marca = marca;
        }

        public void SetVersion(string version)
        {
            this.version = version;
        }

        public string GetCaracteristicas()
        {
            return this.marca + ", " + this.version + ", " + this.dimension + ", " + this.color;
        }

        public abstract Celular Clone();
    }
}
