﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatronesdeDiseño.Clases
{
    internal class Smartphone : Celular
    {
        public Smartphone(string marca, string dimension, string color, string version) : base(marca, dimension, color, version)
        {
        }

        public override Celular Clone()
        {
            return (Celular)this.MemberwiseClone();
        }
    }
}
